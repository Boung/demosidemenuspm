//
//  SideMenuViewController.swift
//  DemoSideMenuSPM
//
//  Created by Ly Boung on 7/9/23.
//

import UIKit
import SideMenuSPM

class SideMenuViewController: UIViewController {
  
  private var tableView: UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = .white
    tableView.translatesAutoresizingMaskIntoConstraints = false
    
    return tableView
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    prepareLayouts()
  }
  
  private func prepareLayouts() {
    view.backgroundColor = .orange
    
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(tableView)
    tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 0).isActive = true
    tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
    tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
  }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
    cell.textLabel?.text = "Menu Row \(indexPath.row + 1)"
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // MARK: - Post action to listen -> Sending IndexPath.row as payload for listener to handle accordingly.
    SideMenuManager.shared.postValueChangeHandler(indexPath.row)
  }
}
