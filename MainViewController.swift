//
//  MainViewController.swift
//  DemoSideMenuSPM
//
//  Created by Ly Boung on 7/9/23.
//

import UIKit
import SideMenuSPM

class MainViewController: UIViewController {
  
  lazy var label = UILabel()
  lazy var descriptionLabel = UILabel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
   
    prepareLayouts()
    addMenuIcon()
    
    SideMenuManager.shared.setValueChangedObserver { payload in
      guard let selectedMenuIndex = payload as? Int else { return }
      
      SideMenuManager.shared.toggleMenuState()
      self.descriptionLabel.text = "Selected Menu Index: \(selectedMenuIndex)"
    }
  }

  private func prepareLayouts() {
    view.backgroundColor = .white
    
    label.text = "-> You can slide to open menu"
    label.textAlignment = .left
    label.font = .systemFont(ofSize: 15)
    label.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(label)
    label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
    label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    
    descriptionLabel.text = "This value will update"
    descriptionLabel.textAlignment = .center
    descriptionLabel.font = .boldSystemFont(ofSize: 15)
    descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    
    view.addSubview(descriptionLabel)
    descriptionLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 16).isActive = true
    descriptionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
  }
  
  private func addMenuIcon() {
    let button = UIBarButtonItem(image: .init(systemName: "rectangle.leftthird.inset.filled"), style: .done, target: self, action: #selector(toggleMenu))
    
    navigationItem.leftBarButtonItem = button
    navigationController?.navigationBar.tintColor = .white
  }
  
  @objc private func toggleMenu() {
    SideMenuManager.shared.toggleMenuState()
  }
}

