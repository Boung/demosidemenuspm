//
//  AppDelegate.swift
//  DemoSideMenuSPM
//
//  Created by Ly Boung on 7/9/23.
//

import UIKit
import SideMenuSPM

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    configRootViewController()
    return true
  }
  
  private func configRootViewController() {
    if #available(iOS 15, *) {
      let appearance = UINavigationBarAppearance()
      appearance.configureWithOpaqueBackground()
      appearance.backgroundColor = .orange
      appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
      UINavigationBar.appearance().standardAppearance = appearance
      UINavigationBar.appearance().scrollEdgeAppearance = appearance
    }
    
    let controller = MainViewController()
    controller.title = "Main Controller"
    let navVC = UINavigationController(rootViewController: controller)
    
    window = UIWindow()
    window?.rootViewController = SideMenuManager.shared.initializeSideMenuController(mainVC: navVC, menuVC: SideMenuViewController(), tapDimissMenu: true, direction: .leftToRight)
    window?.makeKeyAndVisible()
  }
}
